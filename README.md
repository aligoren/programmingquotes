Programlama Alıntıları
---------------------------------
Dosyadan satır satır okunan alıntılardan rastgele olarak bir değer döndürür. **Alıntıları Türkçe diline çevirip bana yollamak isteyenler** ya da farklı alıntı önerisi olanlar **goren.ali@yandex.com** adresine mail atabilirler.

Kullanımı şöyle
-------------------------
Eğer kütüphaneyi projenize dahil ederseniz

~~~~{.python}
# ProgrammingQuotes sınıfını quote değişkenine ata
quote = ProgrammingQuotes()
# Başlık çağırabilirsiniz. >>> Programming Quotes şeklinde gelir
quote.print_title()
# Burada da rastgele şekilde bir söz çekiliyor.
print(quote.print_quotes())
~~~~

# Örnek Kullanımı
~~~~{.shell}
python main.py

>>> Programming Quotes

UNIX was not designed to stop its users from doing stupid things, as that would
also stop them from doing clever things. - Doug Gwyn

python main.py

>>> Programming Quotes

And folks, let's be honest. Sturgeon was an optimist. Way more than 90% of code
is crap. - viro
~~~~

Alıntılar [bu adresten](http://quotes.cat-v.org/programming/) yapılıyor.

İyi kullanımlar.