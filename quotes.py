# -*- coding: utf-8 -*-

# Quotes by: http://quotes.cat-v.org/programming/

import sys
import random
from unidecode import unidecode

if __name__ != '__main__':
    print("Not a module!")
    sys.exit(1)

class ProgrammingQuotes:
    """ Get random programming quotes """
    def __init__(self):
        self.print_title
        self.print_quotes
        
    def print_title(self):
        """ Print Title """
        print(">>> Programming Quotes\n")
    
    def print_quotes(self):
        """ Print Quotes. random lib for random returns, unidecode lib for windows console vs. """
        quoteLine = random.choice(open("quotes.txt").readlines())
        
        quotes = unidecode(quoteLine)
        
        return quotes


quote = ProgrammingQuotes()
quote.print_title()
print(quote.print_quotes())